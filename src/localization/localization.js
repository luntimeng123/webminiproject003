

// ES6 module syntax
import LocalizedStrings from 'react-localization';

export const strings = new LocalizedStrings({

    en: {

        home: "Home",
        add: "Add",
        name: "Name",
        image: "Image",
        action: "Action",
        view: "view",
        delete: "delete",
        edit: "edit",
        author: "Author",
        lang: "Language",
        authorList: "Author List",
        changeImage:"change image",
        articleList:"Article List"
    },

    kh: {

        home: "ផ្ទះ",
        add: "បញ្ជូល",
        name: "ឈ្មោះ",
        image: "រូបភាព",
        action: "សកម្មភាព",
        view: "មើល",
        delete: "លុប",
        edit: "កែប្រែ",
        author: "អ្នកនិពន្ធ",
        lang: "ភាសា",
        authorList: "តារាង​ អ្នកនិពន្ធ",
        changeImage : "កែប្រែរូបភាព",
        articleList:"តារាង​ អត្ថបទ"
    },

    kr: {

        home: "집",
        add: "더하다 ",
        name: "이름 ",
        image: "영상 ",
        action: "동작 ",
        view: "전망",
        delete: "지우다 ",
        edit: "편집하다",
        author: "저자 ",
        lang: "언어 ",
        authorList: "저자 목록 ",
        changeImage:"이미지 변경",
        articleList:"기사 목록 "

    }
});