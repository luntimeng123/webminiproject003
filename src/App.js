import React,{ useEffect, useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './views/Home'
import Menu from './views/header/Menu'
import Add from './views/Add'
import View from './component/View'
import Update from './component/Update'
import AuthorHome from './views/author/AuthorHome'
import ViewAuthor from './component/author/ViewAuthor'
import UpdateAuthor from './component/author/UpdateAuthor'
import { LangContext } from './context/langContext'
import MyCard from './component/MyCard'





export default function App() {

    const [lang, setLang] = useState()


    return (

        <LangContext.Provider value={{lang,setLang}}>
            <Router>
                <Menu />
                <Switch>
                    <Route path="/home" render={() => <Home />} />
                    <Route path="/card" render={()=> <MyCard/>}/>
                    <Route path="/view/:id" render={() => <View />} />
                    <Route path="/add" render={() => <Add />} />
                    <Route path="/update/:id" render={() => <Update />} />
                    <Route path="/author" render={() => <AuthorHome />} />
                    <Route path="/auth/:id" render={() => <ViewAuthor />} />
                    <Route path="/auth_edit/:id" render={() => <UpdateAuthor />} />
                </Switch>
            </Router>
        </LangContext.Provider>

    )
}
