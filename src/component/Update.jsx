import React, { useEffect, useState } from 'react'
import { Form, Card, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'
import { fetchArticleById } from '../redux/action/articleAction'
import { insert_article, insert_Image, update_article } from '../service/article.service'
import swal from 'sweetalert';



export default function Update() {



    const [title, setTitle] = useState('')
    const [des, setDesc] = useState('')

    const [imageurl, setImageURL] = useState('')
    const [imageFile, setImageFile] = useState(null)


    //const dispatch = useDispatch()
    const dispatch = useDispatch()
    const {id} = useParams()
    const history = useHistory()



    useEffect(() => {

        if(id){
        dispatch(fetchArticleById(id)).then(result=>{

            setTitle(result.payload.title)
            setDesc(result.payload.description)
            setImageURL(result.payload.image)

        })
    }

    }, [])


    const insertData = async (e) => {

        e.preventDefault();

        let url = imageFile && await insert_Image(imageFile)

        let objArticle = {
            title,
            description: des,
            image: url ? url : imageurl
        }


        swal({
            title: "Are you sure?",
            text: "you want to update this artcle",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {
                if (willDelete) {

                    let articleMessage = await update_article(id,objArticle)
                    history.push('/home')

                    swal(articleMessage, {
                        icon: "success"
                    });

                } else {

                    swal("you didn't update anything!");

                }
            });

    }




    return (




        <div className="container">
            <br />
            <h1>Update</h1>
            <br />

            <div className="row">
                <div className="col-lg-7">
                    <Form>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Title</Form.Label>
                            <Form.Control
                                type="text"
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                                placeholder="title"
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <br />
                        <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Describtion</Form.Label>
                            <Form.Control
                                value={des}
                                onChange={(e) => setDesc(e.target.value)}
                                as="textarea" rows={3}
                            />
                        </Form.Group>

                        <br />
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={insertData}
                        >
                            Submit
                        </Button>

                    </Form>
                </div>

                <div className="col-lg-5">
                    <Card>
                        <label htmlFor="myfile">Change image
                            <Card.Img src={imageurl} className="w-100" />
                        </label>
                        <Card.Body>
                            <input
                                id="myfile"
                                type="file"
                                onChange={(e) => {
                                    //get file image from our local computer and convert it to url
                                    let url = URL.createObjectURL(e.target.files[0]);
                                    //set it state
                                    setImageURL(url);
                                    //set it to imgFile
                                    setImageFile(e.target.files[0]);
                                }}
                                style={{
                                    display: "none",
                                    width: "100%",
                                    height: "50vh",
                                    objectFit: "cover"
                                }}
                            />
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </div>
    )
}
