import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAuthorById } from '../../redux/action/authorAction'

export default function ViewAuthor() {



    const dispatch = useDispatch()
    const { author } = useSelector(state => state.authorReducer)
    const { id } = useParams()

    useEffect(() => {

        dispatch(fetchAuthorById(id))
        console.log(author)

    }, [])

    return (

        <div className="container">
            <br />
            <h1>{author.name}</h1>
            <br />
            <h3>{author.email}</h3>
            <br />
            <img
                src={author.image}
                style={{
                    width:"50%",
                    height:"100vh",
                    objectFit:"cover"
                }}
            />
        </div>
        
    )
}
