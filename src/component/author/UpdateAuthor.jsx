import React, { useState } from 'react'
import { useEffect } from 'react'
import { Form, Card, Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'
import { fetchAuthorById } from '../../redux/action/authorAction'
import { insert_Image } from '../../service/article.service'
import swal from 'sweetalert';
import { update_author } from '../../service/author.service'



export default function UpdateAuthor() {


    const [name, setName] = useState('')
    const [email, setEmail] = useState('')



    const [imageurl, setImageURL] = useState('')
    const [imageFile, setImageFile] = useState('')



    const dispatch = useDispatch()
    const { id } = useParams()
    const history = useHistory()




    useEffect(() => {

        dispatch(fetchAuthorById(id)).then(auth => {
            setName(auth.payload.name)
            setEmail(auth.payload.email)
            setImageURL(auth.payload.image)
        })

    }, [])





    const updateAuthor = async (e) => {

        e.preventDefault();

        let url = imageFile && await insert_Image(imageFile)

        const objAuthor = {

            name,
            email,
            image : url?url : imageurl

        }

        swal({
            title: "Are you sure?",
            text: "you want to update this author",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {
                if (willDelete) {

                    let authorMessage = await update_author(id,objAuthor);
                    
                    history.push("/author")

                    swal(authorMessage, {
                        icon: "success",
                    });

                } else {

                    swal("you didn't update anything!");

                }
            });

    }




    return (

        <div className="container">
            <br/>
            <div className="row">
                <div className="col-lg-7">
                    <Form>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" id="name" value={name} onChange={(e) => setName(e.target.value)} placeholder="Name" />
                            <Form.Text className="text-muted">
                                We'll never share your name with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <br />
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="text" id="email" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="email@.com" />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>


                        <br />
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={updateAuthor}
                        >
                            Submit
                        </Button>

                    </Form>
                </div>

                <div className="col-lg-5">
                    <Card>
                        <label htmlFor="myfile">Change image
                            <Card.Img src={imageurl} className="w-100"
                                style={{
                                    width: "100%",
                                    height: "25vh",
                                    objectFit: "cover"
                                }} />
                        </label>
                        <Card.Body>
                            <input
                                id="myfile"
                                type="file"
                                onChange={(e) => {
                                    //get file image from our local computer and convert it to url
                                    let url = URL.createObjectURL(e.target.files[0]);
                                    //set it state
                                    setImageURL(url);
                                    //set it to imgFile
                                    setImageFile(e.target.files[0]);
                                }}
                                style={{
                                    display: "none",
                                    width: "100%",
                                    height: "50vh",
                                    objectFit: "cover"
                                }}
                            />
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </div>
    )
}
