import React, { useEffect, useState } from 'react'
import { Card, Button, Spinner } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { deleteArticleById, fetchAllArticles, fetchArticleById } from '../redux/action/articleAction'
import ReactPaginate from 'react-paginate';
import swal from 'sweetalert';
import { strings } from '../localization/localization'


export default function MyCard() {




    const [page, setPage] = useState(1)
    const [isArtLoading, setIsArtLoading] = useState(true)


    const dispatch = useDispatch()
    const { articles, isLoading, totalpage } = useSelector(state => state.articleReducer)




    useEffect(() => {
        dispatch(fetchAllArticles(page)).then(() => {
            setIsArtLoading(false)
        })
    }, [])





    const onPageChange = async (obj) => {

        setIsArtLoading(true)

        //because onPageChange store a obj that in clude selected property,this property have data as number
        await dispatch(fetchAllArticles(obj.selected + 1))
        console.log("obj ", obj.selected + 1)

        setIsArtLoading(false)

    }




    const deleteArticle = (id) => {

        setIsArtLoading(true)
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {

                if (willDelete) {

                    const result = await dispatch(deleteArticleById(id))
                    setIsArtLoading(false)

                    swal(result, {
                        icon: "success",
                    });

                } else {
                    setIsArtLoading(false)
                    swal("You didn't delete anything!");

                }

            });

    }





    return (
        <div className="container">
            <div style={{ display: "flex", flexFlow: "row wrap" }}>
                {
                    isArtLoading ? (<b><h1
                        style={{
                            margin: "150px 650px"
                        }}
                    ><Spinner animation="border" /></h1></b>) : (
                        articles.map((item, index) => (
                            <Card style={{ width: '18rem', margin: "10px" }} key={index}>
                                <Card.Img variant="top"
                                    src={item.image ? item.image : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4YHTOvVRuHNktlGR-axAbIsXozzwtTWvdWg&usqp=CAU"}
                                    style={{
                                        width: "100%",
                                        height: "25vh",
                                        objectFit: "cover"
                                    }}
                                />
                                <Card.Body>
                                    <Card.Title>{item.title}</Card.Title>
                                    <Card.Text>
                                        {item.description}
                                    </Card.Text>

                                    <Button as={Link} to={`/view/${item._id}`} variant="primary">{strings.view}</Button>{" "}
                                    <Button variant="danger" onClick={() => deleteArticle(item._id)}>{strings.delete}</Button>{" "}
                                    <Button as={Link} to={`/update/${item._id}`} variant="warning">{strings.edit}</Button>

                                </Card.Body>
                            </Card>
                        ))
                    )
                }
            </div>

            <ReactPaginate
                pageCount={totalpage}
                onPageChange={onPageChange}
                containerClassName="pagination pagination-sm justify-content-center"
                pageClassName="page-item btn btn-outline-primary btn-sm"
                pageLinkClassName="page-link"
                previousClassName="page-item btn btn-outline-primary btn-sm"
                previousLinkClassName="page-link"
                nextLinkClassName="page-link"
                nextClassName="page-item btn btn-outline-primary btn-sm"
                activeClassName="active"
            />

        </div>
    )
}
