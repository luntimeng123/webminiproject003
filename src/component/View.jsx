import React, { useEffect } from 'react'
import { } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { fetchArticleById } from '../redux/action/articleAction'



export default function View() {

    const param = useParams()
    const dispatch = useDispatch()
    const { article } = useSelector(state => state.articleReducer)




    useEffect(() => {
        dispatch(fetchArticleById(param.id)).then(result=>{
            console.log(result.payload)
        })
    }, [])




    return (

        <div className="container">
            <div className="row">
                <div className="col-lg-8">
                    <br />
                    <h1>{article.title}</h1>
                    <img
                        src={article.image ? article.image : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4YHTOvVRuHNktlGR-axAbIsXozzwtTWvdWg&usqp=CAU"}
                        style={{
                            width: "100%",
                            height: "50vh",
                            objectFit: "cover"
                        }}
                    />
                    <p style={{ fontSize: "25px" }}>{article.description ? article.description : "Some quick example text to build on the card title and make up the bulk of the card's content."}</p>

                </div>
                <div className="col-lg-4">
                <br />
                <br/>
                <br/>
                    <h1>Comment</h1>
                </div>
            </div>

        </div>
    )
}
