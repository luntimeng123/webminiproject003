import React, { useEffect } from 'react'
import {Button} from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllCategory } from '../redux/action/categoryAction'




export default function MyCategory() {

    const dispatch = useDispatch()
    const {category} = useSelector(state => state.categoryReducer)


    useEffect(()=>{
        dispatch(fetchAllCategory())
    },[])

    return (



        <div className="container" style={{display:"flex",flexFlow:"row wrap"}}>
            {
                category.map((item,index)=>(
                    <Button variant="primary" key={index} style={{margin:"5px"}}>{item.name}</Button>
                ))
            }
        </div>
    )
}
