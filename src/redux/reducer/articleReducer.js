import { DELETE_ARTICLE_BY_ID, FETCH_ALL_ARTICLES, FETCH_ARTICLE_BY_ID} from "../action/articleAction";




const initState = {

    articles : [],
    article : [],
    isLoading : true,
    totalpage : null

}




export default function articleReducer(state = initState,{type,payload,isLoading,totalpage}) {

    switch (type) {
        
        case FETCH_ALL_ARTICLES:
            
            return {
                ...state,
                articles : payload,
                isLoading : isLoading,
                totalpage : totalpage
            }

        case FETCH_ARTICLE_BY_ID:

            return {
                ...state,
                article : payload
            }

        case DELETE_ARTICLE_BY_ID:

            return {

                ...state,
                articles : state.articles.filter(item=>item._id != payload)

            }
    
        default:
            return state
    }

}