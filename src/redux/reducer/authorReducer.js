import { DELETE_AUTHOR_BY_ID, FETCH_ALL_AUTHORS, FETCH_AUTHOR_BY_ID } from "../action/authorAction";



const initState = {

    authors: [],
    author : {},
    isLoading : true

}






export default function authorReducer(state = initState, { type, payload ,isLoading }) {

    switch (type) {

        case FETCH_ALL_AUTHORS:

            return {

                ...state,
                authors: payload,
                isLoading : isLoading
            }

        case FETCH_AUTHOR_BY_ID :

            return {
                ...state,
                author : payload
            }

        case DELETE_AUTHOR_BY_ID:

            return {
                ...state,
                authors : state.authors.filter(item => item._id !== payload)
            }

        default:

            return state
    }
}