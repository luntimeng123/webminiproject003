import { FETCH_ALL_CATEGORY } from "../action/categoryAction";


const initState = {
    category : []
}



export default function categoryReducer (state = initState ,{type,payload}) {

    switch (type) {

        case FETCH_ALL_CATEGORY :
            
            return {

                ...state,
                category : payload
            }
    
        default:
            
            return state
    }
}