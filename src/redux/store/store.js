import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from 'redux-thunk'
import articleReducer from '../reducer/articleReducer'
import categoryReducer from "../reducer/categoryReducer";
import authorReducer from "../reducer/authorReducer";




const rootReducer = combineReducers ({
        articleReducer,categoryReducer,authorReducer
    })





export const store = createStore(rootReducer,applyMiddleware(thunk))