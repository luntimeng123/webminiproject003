import { add_author, delete_author, fetch_all_author, fetch_author_by_id } from "../../service/author.service"
import { DELETE_ARTICLE_BY_ID } from "./articleAction"
import swal from 'sweetalert';





export const FETCH_ALL_AUTHORS = "FETCH_ALL_AUTHORS"
export const FETCH_AUTHOR_BY_ID = "FETCH_AUTHOR_BY_ID"
export const DELETE_AUTHOR_BY_ID = "DELETE_AUTHOR_BY_ID"






//***fetch all author */

export const fetchALLAuthor = () => async dp => {

    const result = await  fetch_all_author()

    return dp ({

        type : FETCH_ALL_AUTHORS,
        payload : result.data.data,
        isLoading : false

    })
}





//****fetch author by id */

export const fetchAuthorById = (id) => async dp =>{

    const result = await fetch_author_by_id(id)

    return dp ({

        type : FETCH_AUTHOR_BY_ID,
        payload : result

    })
}




//***delete author by id */

export const deleteAuthor = (id) => {

    return async dispatch => {

        const result = await delete_author(id)

        dispatch({

            type: DELETE_ARTICLE_BY_ID,
            payload : result

        })
    }
}