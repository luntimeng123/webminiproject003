import { fetch_all_category } from "../../service/categories.service"

export const FETCH_ALL_CATEGORY = "FETCH_ALL_CATEGORY"





//*****fetch all category */

export const fetchAllCategory = () => async dp => {

    const result = await fetch_all_category()

    return dp ({

        type : FETCH_ALL_CATEGORY,
        payload : result.data.data
        
    })
}