import { delete_article, fetch_all_articles, fetch_article_by_id } from "../../service/article.service"
import { API } from "../../service/api/API";




export const FETCH_ALL_ARTICLES = "FETCH_ALL_ARTICLES"
export const FETCH_ARTICLE_BY_ID = "FETCH_ARTICLE_BY_ID"
export const DELETE_ARTICLE_BY_ID = "DELETE_ARTICLE_BY_ID"





//****delete article by id */

export const deleteArticleById = (id) => {

    return async dispatch => {


        const result = await delete_article(id)

        dispatch({
            type: DELETE_ARTICLE_BY_ID,
            payload: id
        })
    }
}





//*****fetch all article */

export const fetchAllArticles = (page) => {

    return async dispatch => {

        const result = await fetch_all_articles(page)
        console.log("totalPage", result.total_page)

        dispatch({
            type: FETCH_ALL_ARTICLES,
            payload: result.data,
            totalpage: result.total_page,
            isLoading: false
        })
    }
}




//*****fetch article by id */

export const fetchArticleById = (id) => async dp => {

    const result = await API.get("/articles/" + id)

    return dp({
        type: FETCH_ARTICLE_BY_ID,
        payload: result.data.data
    })

}


