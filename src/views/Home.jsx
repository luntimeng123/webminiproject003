import React from 'react'
import { } from 'react-bootstrap'
import MyCard from '../component/MyCard'
import MyCategory from '../component/MyCategory'
import { strings } from '../localization/localization'

export default function Home() {



    return (

        <div className="container">
            <br />
            <h1>{strings.articleList}</h1>
            <hr />
            <MyCategory/>
            <br />
            <MyCard />
        </div>
    )
}
