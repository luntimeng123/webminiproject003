import React, { useState } from 'react'
import { Form, Button, Card, Spinner } from 'react-bootstrap'
import { insert_article, insert_Image } from '../service/article.service'
import swal from 'sweetalert';




export default function Add() {


    const [title, setTitle] = useState()
    const [des, setDesc] = useState()
    const [loading, setLoading] = useState(false)

    const [imageurl, setImageURL] = useState("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzSc0E_-ezcw1juku7x_q9rIVtGDEFGDsZnA&usqp=CAU")
    const [imageFile, setImageFile] = useState(null)






    const insertData = async (e) => {
        setLoading(true)
        e.preventDefault();

        let url = imageFile && await insert_Image(imageFile)

        let objArticle = {
            title,
            description: des,
            image: url ? url : "https://sutvacha.s3.amazonaws.com/media/public/product/no-image-available.png"
        }


        swal({
            title: "Are you sure?",
            text: "you want to add this artcle",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {
                if (willDelete) {

                    let articleMessage = await insert_article(objArticle);
                    setLoading(false)
                    swal(articleMessage, {
                        icon: "success",
                    });

                } else {
                    setLoading(false)
                    swal("you didn't add anything!");

                }
            });

    }







    return (


        <div className="container">
            <br />
            <br />
            {
                loading ? (<h1
                    style={{
                        margin: "250px 650px"
                    }}
                ><b><Spinner animation="border" /></b></h1>) :
                    (
                        <div className="row">
                            <div className="col-lg-7">
                                <Form>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Title</Form.Label>
                                        <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Title" />
                                        <Form.Text className="text-muted">
                                            We'll never share your email with anyone else.
                                        </Form.Text>
                                    </Form.Group>

                                    <br />
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Describtion</Form.Label>
                                        <Form.Control value={des} onChange={(e) => setDesc(e.target.value)} as="textarea" rows={3} />
                                    </Form.Group>

                                    <br />
                                    <Button
                                        variant="primary"
                                        type="submit"
                                        onClick={insertData}
                                    >
                                        Submit
                                    </Button>

                                </Form >
                            </div >

                            <div className="col-lg-5">
                                <Card>
                                    <label htmlFor="myfile">Change image
                                        <Card.Img src={imageurl} className="w-100" />
                                    </label>
                                    <Card.Body>
                                        <input
                                            id="myfile"
                                            type="file"
                                            onChange={(e) => {
                                                //get file image from our local computer and convert it to url
                                                let url = URL.createObjectURL(e.target.files[0]);
                                                //set it state
                                                setImageURL(url);
                                                //set it to imgFile
                                                setImageFile(e.target.files[0]);
                                            }}
                                            style={{
                                                display: "none",
                                                width: "100%",
                                                height: "50vh",
                                                objectFit: "cover"
                                            }}
                                        />
                                    </Card.Body>
                                </Card>
                            </div>
                        </div >
                    )
            }
        </div >
    )
}
