import React, { useEffect, useState, useContext } from 'react'
import { Navbar, Nav, Dropdown } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { LangContext } from '../../context/langContext'
import { strings } from '../../localization/localization'


export default function Menu() {



    const [language, setlanguage] = useState([
        {
            title: "ខ្មែរ",
            key: "kh"
        },
        {
            title: "English",
            key: "en"
        },
        {
            title: "한국",
            key: "kr"
        }
    ])

    const [update, setUpdate] = useState({})

    const context = useContext(LangContext)


    useEffect(() => {

        strings.setLanguage(localStorage.getItem("lang"))
        context.setLang(localStorage.getItem("lang"))

    }, [])


    const changeLang = (lang) => {
        strings.setLanguage(lang)
        context.setLang(lang)
        localStorage.setItem("lang", lang)
    }


    return (


        <div className="container">
            <Navbar bg="light" variant="light">
                <Navbar.Brand href="#home">Navbar</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/home">{strings.home}</Nav.Link>
                    <Nav.Link as={Link} to="/add">{strings.add}</Nav.Link>
                    <Nav.Link as={Link} to="/author">{strings.author}</Nav.Link>
                </Nav>{' '}
                <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        {strings.lang}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>

                        {
                            language.map((lang, index) => (
                                <Dropdown.Item
                                    key={index}
                                    onClick={() => changeLang(lang.key)}
                                > {lang.title}
                                </Dropdown.Item>
                            ))
                        }

                    </Dropdown.Menu>
                </Dropdown>
            </Navbar>
        </div>
    )
}
