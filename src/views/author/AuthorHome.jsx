import React, { useState } from 'react'
import { useEffect } from 'react'
import { Table } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { deleteAuthor, fetchALLAuthor } from '../../redux/action/authorAction'
import { Button, Form, Card, Spinner } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { insert_Image } from '../../service/article.service'
import swal from 'sweetalert';
import { add_author } from '../../service/author.service'
import ReactLoading from 'react-loading';
import { strings } from '../../localization/localization'




export default function AuthorHome() {





    const dispatch = useDispatch()
    const { authors, isLoading } = useSelector(state => state.authorReducer)
    const [ref, setRef] = useState(0)


    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [loading, setLoading] = useState(true)

    const [imageurl, setImageURL] = useState('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZUX7zo1yYFaBeOYIcOfcgwnULvpM7YqzXxA&usqp=CAU')
    const [imageFile, setImageFile] = useState('')







    const Loading = () => (
        <ReactLoading type={"cylon"} color={"#000000"} height={'70%'} width={'70%'} />
    );






    useEffect(() => {

        dispatch(fetchALLAuthor()).then(()=>{
            setLoading(false)
        })
        refreshPage()

    }, [ref])








    const deleteAuthorById = (id) => {

        setLoading(true)

        swal({
            title: "Are you sure?",
            text: "you want to delete this author",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {
                if (willDelete) {

                    let authorMessage = await dispatch(deleteAuthor(id))

                    setLoading(false)
                    setRef(ref + 1)

                    swal(authorMessage, {
                        icon: "success",
                    });

                } else {

                    swal("you didn't delete anything!");
                    setLoading(false)

                }
            });

    }






    const addAuthor = async (e) => {

        e.preventDefault();

        setLoading(true)

        let url = imageFile && await insert_Image(imageFile)

        const objAuthor = {

            name,
            email,
            image: url ? url : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZUX7zo1yYFaBeOYIcOfcgwnULvpM7YqzXxA&usqp=CAU'

        }

        swal({
            title: "Are you sure?",
            text: "you want to add this author",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {
                if (willDelete) {

                    let authorMessage = await add_author(objAuthor);

                    setLoading(false)
                    setRef(ref + 1)

                    swal(authorMessage, {
                        icon: "success",
                    });

                } else {

                    swal("you didn't add anything!");
                    setLoading(false)

                }
            });

    }


    const refreshPage = () => {
        document.getElementById('name').value = ""
        document.getElementById('email').value = ""
        setImageURL('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZUX7zo1yYFaBeOYIcOfcgwnULvpM7YqzXxA&usqp=CAU')
    }


    return (


        <div className="container">
            <br />
            <br />

            <div className="row">
                <div className="col-lg-7">
                    <Form>

                        <Form.Group>
                            <Form.Label>{strings.name}</Form.Label>
                            <Form.Control type="text" id="name" onChange={(e) => setName(e.target.value)} placeholder="Name" />
                            <Form.Text className="text-muted">
                                We'll never share your name with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <br />
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="text" id="email" onChange={(e) => setEmail(e.target.value)} placeholder="email@.com" />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>


                        <br />
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={addAuthor}
                        >
                            {strings.add}
                        </Button>

                    </Form>
                </div>

                <div className="col-lg-5">
                    <Card>
                        <label htmlFor="myfile">{strings.changeImage}
                            <Card.Img src={imageurl} className="w-100"
                                style={{
                                    width: "100%",
                                    height: "25vh",
                                    objectFit: "cover"
                                }} />
                        </label>
                        <Card.Body>
                            <input
                                id="myfile"
                                type="file"
                                onChange={(e) => {
                                    //get file image from our local computer and convert it to url
                                    let url = URL.createObjectURL(e.target.files[0]);
                                    //set it state
                                    setImageURL(url);
                                    //set it to imgFile
                                    setImageFile(e.target.files[0]);
                                }}
                                style={{
                                    display: "none",
                                    width: "100%",
                                    height: "50vh",
                                    objectFit: "cover"
                                }}
                            />
                        </Card.Body>
                    </Card>
                </div>
            </div>




            <br />
            <br />
            <br />



            <h1>{strings.authorList}</h1>
            {
                loading ? (<h1
                    style={{
                        margin: "100px 600px"
                    }}
                ><b>{Loading()}</b></h1>)
                    :
                    (<>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{strings.name}</th>
                                    <th>Email</th>
                                    <th>{strings.image}</th>
                                    <th>{strings.action}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    authors.map((item, index) => (
                                        <tr key={index}>
                                            <td>{item._id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td style={{ width: "250px" }}>
                                                <img src={item.image}
                                                    style={{
                                                        width: "100%",
                                                        height: "25vh",
                                                        objectFit: "cover"
                                                    }}
                                                />
                                            </td>
                                            <td style={{ display: "flex", justifyContent: "space-around" }}>
                                                <Button as={Link} to={`/auth/${item._id}`} variant="primary">{strings.view}</Button>{' '}
                                                <Button variant="danger" onClick={() => deleteAuthorById(item._id)}>{strings.delete}</Button>{' '}
                                                <Button as={Link} to={`/auth_edit/${item._id}`} variant="warning">{strings.edit}</Button>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </Table>

                    </>)
            }
        </div>
    )
}
