
import { API } from "./api/API"



//****fetch all author */

export const fetch_all_author = async () => {

    try {
        
        const result = await API.get("/author")

        return result

    } catch (error) {
        console.log("your error ",error)
    }
}





//****fetch author by id */

export const fetch_author_by_id = async (id) => {

    try {
        
        const result = await API.get("/author/"+id)
        return result.data.data

    } catch (error) {

        console.log("your error ",error)
    }
}




//****delete author */

export const delete_author = async (id) => {

    try {
        
        const result = await API.delete("/author/"+id)

        return result.data.message

    } catch (error) {
        console.log("your error ",error)
    }
}




//****add author */

export const add_author = async (auth) => {

    try {
        
        const result = await API.post("/author",auth)

        return result.data.message

    } catch (error) {
        console.log("your error ",error)
    }
}






//***update author */

export const update_author = async (id,auth) => {

    try {
        
        const result = await API.put("/author/"+id,auth)

        return result.data.message

    } catch (error) {
        console.log("your error ",error)
    }
}