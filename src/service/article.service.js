
import { API } from './api/API'



//***update article */

export const update_article = async (id,article) => {

    try {
        const result = await API.patch("/articles/"+ id,article)

        return result.data.data
        
    } catch (error) {
        console.log("maybe can't find your input id",error)
    }
}



//****fetch all articles */

export const fetch_all_articles = async (page) => {

    try {

        const result = await API.get(`/articles?page=${page}&size=2`)

        return result.data

    } catch (error) {
        console.log("allArticlError", error)
    }

}


//****fetch article by id */

export const fetch_article_by_id = async (id) => {

    try {

        const result = await API.get("/articles/" + id)
        return result

    } catch (error) {
        console.log("articleVyId", error)
    }
}





//****delete article */
export const delete_article = async (id) => {

    try {
        
        const result = await API.delete("/articles/" + id)

        return result.data.message

    } catch (error) {
        console.log("delete error",error)
    }

}








//****insert article */

export const insert_article = async (article) => {

    try {
        
        const result = await API.post("/articles",article)

        return result.data.message

    } catch (error) {
        console.log("insertArticle", error)
    }
}



//uploade image to api
export const insert_Image = async (imageFile) => {
    try{
        //flow upload image to api
  
    let formData = new FormData();
    formData.append("image", imageFile);
  
    let respone = await API.post("images", formData);
  
    return respone.data.url;

    }catch(error){

        console.log('you should handle: ',error);

    }
  };
  